package com.company.Core;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DiningPhilosophers
{
    private class Seat
    {
        public boolean LFork = true;
        public boolean RFork = true;
    }

    private Lock Lock = new ReentrantLock();

    private final Seat[] Seats = new Seat[5];

    public DiningPhilosophers()
    {
        for (int i = 0; i < 5; i++)
        {
            Seats[i] = new Seat();
        }
    }

    // call the run() method of any runnable to execute its code
    public void wantsToEat(int philosopher,
                           Runnable pickLeftFork,
                           Runnable pickRightFork,
                           Runnable eat,
                           Runnable putLeftFork,
                           Runnable putRightFork) throws InterruptedException
    {
        boolean done = false;

        while(!done)
        {
            this.Lock.lock();

            if(Seats[philosopher].LFork && Seats[philosopher].RFork)
            {
                locker(philosopher, false);

                this.Lock.unlock();

                pickLeftFork.run();
                pickRightFork.run();
            }
            else
            {
                this.Lock.unlock();

                Thread.sleep(5);
                continue;
            }

            eat.run();

            putLeftFork.run();
            putRightFork.run();

            this.Lock.lock();

                locker(philosopher, true);

            this.Lock.unlock();

            done = true;
        }
    }


    private void locker(int user, boolean status)
    {
        Seats[user].LFork = status;
        Seats[user].RFork = status;

        var neighbors = getNeighbors(user);

        Seats[neighbors[0]].LFork = status;
        Seats[neighbors[1]].RFork = status;
    }

    private int[] getNeighbors(int id)
    {
        return new int[]
        {
            id == 0 ? 4 : id - 1,
            id == 4 ? 0 : id + 1
        };
    }
}